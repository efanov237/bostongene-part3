# Part 3. Yandex Translate  
У Яндекс переводчика (translate.yandex.ru) имеется REST API. Необходимо реализовать консольное
приложение на одном из языков Java8, Groovy или Kotlin, которое переводит введенную
английскую фразу на русский язык, пользуясь предложенным API.
### Build and run
```sh
$ mvn package
$ java -jar target/part3-1.0.jar
```
To exit enter '0'.