import efanov237.bostongene.Translator;
import org.junit.Assert;
import org.junit.Test;

public class TranslatorTests {

    private static final String VALID_KEY = "trnsl.1.1.20180215T213830Z.12f38073a7166db9.b4d36e6933ef082a28650bd1cbb1cbab066e464d";

    @Test
    public void englishToRussian() {
        Translator translator = new Translator(VALID_KEY);
        String input = "Hello, world!";
        String output = translator.translate(input, "en-ru");
        Assert.assertEquals("Привет, мир!", output);
    }

    @Test
    public void russianToEnglish() {
        Translator translator = new Translator(VALID_KEY);
        String input = "Привет, мир!";
        String output = translator.translate(input, "ru-en");
        Assert.assertEquals("Hello world!", output);
    }

    @Test
    public void invalidKey() {
        String invalidKey = "invalid_key";
        Translator translator = new Translator(invalidKey);
        Assert.assertEquals("API key is invalid", translator.translate("",""));
    }
}
