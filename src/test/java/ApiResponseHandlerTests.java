import efanov237.bostongene.ApiResponseHandler;
import efanov237.bostongene.entity.ErrorMessage;
import efanov237.bostongene.entity.TranslatedMessage;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.message.BasicHeader;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApiResponseHandlerTests {

    private Header[] mockedHeaders;
    private HttpResponse mockedResponse;
    private StatusLine mockedStatusLine;
    private ApiResponseHandler handler;

    public ApiResponseHandlerTests() {
        this.mockedHeaders = new Header[] { new BasicHeader("Content-type", "application/json") };
        this.mockedResponse = mock(HttpResponse.class);
        this.mockedStatusLine = mock(StatusLine.class);
        this.handler = new ApiResponseHandler();
    }

    @Test
    public void parseTranslatedMessage() throws IOException {
        when(mockedResponse.getStatusLine()).thenReturn(mockedStatusLine);
        when(mockedStatusLine.getStatusCode()).thenReturn(200);
        when(mockedResponse.getEntity()).thenReturn(
                new InputStreamEntity(
                        new ByteArrayInputStream(
                                "{\"code\":200,\"lang\":\"en-ru\",\"text\":[\"success\"]}".getBytes("UTF-8"))));
        when(mockedResponse.getAllHeaders()).thenReturn(mockedHeaders);
        TranslatedMessage message = (TranslatedMessage) handler.handleResponse(mockedResponse);
        Assert.assertEquals(message.getText(), "success");
    }

    @Test
    public void parseErrorMessage() throws IOException {
        when(mockedResponse.getStatusLine()).thenReturn(mockedStatusLine);
        when(mockedStatusLine.getStatusCode()).thenReturn(401);
        when(mockedResponse.getEntity()).thenReturn(
                new InputStreamEntity(
                        new ByteArrayInputStream(
                                "{\"code\":401,\"message\":\"success\"}".getBytes("UTF-8"))));
        when(mockedResponse.getAllHeaders()).thenReturn(mockedHeaders);
        ErrorMessage errorMessage = (ErrorMessage) handler.handleResponse(mockedResponse);
        Assert.assertEquals(errorMessage.getMessage(), "success");
    }

}
