package efanov237.bostongene;


import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        String key = "trnsl.1.1.20180215T213830Z.12f38073a7166db9.b4d36e6933ef082a28650bd1cbb1cbab066e464d";

        Translator translator = new Translator(key);
        Scanner scanner = new Scanner(System.in, "UTF-8");

        String input = scanner.nextLine();
        while (!"0".equals(input)) {
            if (!input.isEmpty()) {
                System.out.println(translator.translate(input, "en-ru"));
            } else {
                System.err.println("Empty line!");
            }
            input = scanner.nextLine();
        }
    }
}
