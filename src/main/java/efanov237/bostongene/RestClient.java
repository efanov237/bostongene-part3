package efanov237.bostongene;


import efanov237.bostongene.entity.ApiResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class RestClient {

    private static final String SERVICE_SCHEME = "https";
    private static final String SERVICE_HOST = "translate.yandex.net";
    private static final String SERVICE_PATH = "/api/v1.5/tr.json/translate";

    private final String API_KEY;

    RestClient(final String key) {
        this.API_KEY = key;
    }


    public HttpUriRequest buildRequest(String text, String lang) {
        URIBuilder uriBuilder = new URIBuilder();
        try {
            URI uri = uriBuilder.setScheme(SERVICE_SCHEME)
                    .setHost(SERVICE_HOST)
                    .setPath(SERVICE_PATH)
                    .setParameter("key", API_KEY)
                    .setParameter("text", text)
                    .setParameter("lang", lang)
                    .build();
            return new HttpPost(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ApiResponse request(HttpUriRequest request) {
        try {
            HttpClient client = HttpClients.createDefault();
            ApiResponseHandler handler = new ApiResponseHandler();
            return client.execute(request, handler);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}