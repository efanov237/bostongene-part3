package efanov237.bostongene;

import efanov237.bostongene.entity.ApiResponse;
import efanov237.bostongene.entity.ErrorMessage;
import efanov237.bostongene.entity.TranslatedMessage;
import org.apache.http.client.methods.HttpUriRequest;

public class Translator {

    private final RestClient client;

    public Translator(String key) {
        this.client = new RestClient(key);
    }

    public String translate(String translatableString, String lang) {
        HttpUriRequest request = client.buildRequest(translatableString, lang);
        ApiResponse response = client.request(request);
        if (response.getClass().isAssignableFrom(TranslatedMessage.class)) {
            TranslatedMessage message = (TranslatedMessage) response;
            return message.getText();
        } else {
            ErrorMessage error = (ErrorMessage) response;
            return error.getMessage();
        }
    }


}
