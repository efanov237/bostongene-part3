package efanov237.bostongene.entity;

public class TranslatedMessage extends ApiResponse {

    private final String[] text;
    private final String lang;

    TranslatedMessage(int code, String[] text, String lang) {
        super(code);
        this.text = text;
        this.lang = lang;
    }

    public String getText() {
        return text[0];
    }

    public String getLang() {
        return lang;
    }
}
