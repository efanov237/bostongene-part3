package efanov237.bostongene.entity;

public class ApiResponse {

    private final int code;

    ApiResponse(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}
