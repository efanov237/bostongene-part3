package efanov237.bostongene.entity;

public class ErrorMessage extends ApiResponse {

    private final String message;

    public ErrorMessage(int code, String message) {
        super(code);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
