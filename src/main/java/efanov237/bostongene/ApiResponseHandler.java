package efanov237.bostongene;


import com.google.gson.Gson;
import efanov237.bostongene.entity.ApiResponse;
import efanov237.bostongene.entity.ErrorMessage;
import efanov237.bostongene.entity.TranslatedMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

import java.io.IOException;


public class ApiResponseHandler implements ResponseHandler<ApiResponse> {
    @Override
    public ApiResponse handleResponse(HttpResponse response) throws IOException {
        int status = response.getStatusLine().getStatusCode();
        String json = EntityUtils.toString(response.getEntity(), "UTF-8");
        Gson parser = new Gson();
        if (status == 200) {
            return parser.fromJson(json, TranslatedMessage.class);
        } else {
            return parser.fromJson(json, ErrorMessage.class);
        }
    }
}
